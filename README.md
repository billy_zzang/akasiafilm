Train to Busan 2 peninsula full movie kapan rilis. Peninsula sekuel film dari Train to Busan, telah mengumumkan jadwal perilisannya!

"Peninsula" adalah sebuah blockbuster aksi yang berpusat di sekitar sekelompok orang yang tertinggal untuk memperjuangkan hidup mereka di tanah yang hancur. Hal itu terjadi empat tahun setelah film "Train to Busan." Dibintangi oleh Kang Dong Won, Lee Jung Hyun, Kwon Hae Hyo, Kim Min Jae, Goo Kyo Hwan, Kim Do Yoon, Lee Re, dan Lee Ye Won.

Telah dikonfirmasi bahwa "Peninsula" akan tayang di Korea Selatan pada 15 Juli. Dipertengahan bulan ini (Juni), sutradara dan pemain utama melakukan konfrerensi pers untuk berbincang mengenainya film tersebut.

Disutradarai oleh Yeon Sang Ho, film blockbuster Korea Selatan yang paling dinantikan. Baru-baru ini, fil ini diundang ke Festival Film Cannes ke-73 sebagai bagian dari “2020 Official Selection.”

Film Peninsula berlangsung setelah empat tahun wabah zombie di Train to Busan. Semenanjung Korea hancur dan Jung Seok, seorang mantan prajurit yang berhasil melarikan diri ke luar negeri, diberi misi untuk kembali dan tiba-tiba bertemu dengan para penyintas.

https://akasiafilm.blogspot.com/2020/07/train-to-busan-2-peninsula-full-movie.html

UPDATE [09/07/2020]: Jadwal Tayang di Indonesia

Film sekuel dari Train to Busan ini mengumumkan jadwal tayang perdana di Indonesia. Distributor resmi, CBI Pictures pada Kamis (09/07) mengumumkan bahwa film yang dibintangi pemeran terkenal, yaitu Kang Dong Won dan Lee Jung Hyun akan tayang pada bulan Agustus.

Selain mengumumkan berita mengembirakan tersebut, pihak CBI Picture selaku distributor resmi juga mengunggah foto poster "Peninsula" resmi berbhasa Indonesia, yang bertuliskan "Empat Tahun Setelah Train to Busan. Bertahan hidup sampai akhir.".


Update [16/Juli/2020]

Tayang perdana di Korea Selatan pada Rabu kemarin (15/07).

Dari hasil yang dirilis Korean Film Council pada pagi Kamis (16/07), "Peninsula" mendapatkan jumlah penonton hingga 352.930 penonton di hari pertama penayangannya.

Rekor tersebut merupakan rekor tertinggi sejak Korea Selatan memasuki pandemi COVID-19.
https://akasiafilm.blogspot.com/2020/07/drama-backstreet-rookie-dan-its-okay-to.html
Rekor tersebut berhasil mengalahkan rekor sbeelumnya yang diperoleh film "#ALIVE" yang disutradarai oleh Cho Il-hyung, dan dibintangi aktor dan aktris utama papan atas yaitu Yoo Ah-in and Park Shin-hye. "#ALIVE" sendiri meraih rekor 204.071 penonton pada tayangan perdananya.




Drama ‘Backstreet Rookie’ dan ‘It’s Okay To Not Be Okay’ Alami Peningkatan Jumlah Penonton dari episode minggu kemarin.

Sebelumnya It’s Okay To Not Be Okay mendapatkan rating yang stabil selamat tiga minggu berturut-turut. Episode terbaru It’s Okay To Not Be Okay yang tayang pada hari Sabtu (18/07) berhasil mengalami peningkatan rating yang signifikan.

Nielsen Korea merilis laporan bahwa episode terbaru drama yang dibintangi Kim Soo-hyun dan Seo Ye-ji berhasil mendapatkan rata-rata rating nasional sebesar 5,8% dan titik puncaknya 6,2%.

Selain It’s Okay To Not Be Okay, drama yang tayang dengan waktu yang sama, Backstreet Rookie juga mendapatkan peningkatan rating dari sebelumnya, yaitu 5,3% dan 8,4% pada titik puncaknya.
https://akasiafilm.blogspot.com/2020/07/netflix-buka-suara-soal-kingdom-season.html

Di minggu dan waktu yang sama juga, drama King Maker: The Change of Destiny mendapatkan rating yang stabil dengan perolehan 5%, disusul dengan drama Train yang meraih 1%, dan Graceful Friends dengan perolehan rating 3,7%.

Sedangkan untuk drama keluarga, Once Again yang tayang di KBS2 meraih perolehan rating sebesar 23,9% dan 29,4% yang menjadikannya pemimpim dalam minggu kemarin.



Para Pemain Drama ‘Search’ Menghadiri Pembacaan Naskah Pertama yang diselenggarakan di gedung CJ Entertainment.

Pada April lalu, Drama terbaru Search menggelar pembacaan naskah pertama. Para pemeran dan staff drama datang bersama.

Sinopsis Search sendiri, yaitu Koo Dong-jin yang diperankan Jang Doo-yoon dan Son Ye-rim yang diperankan Krystal mencoba bertarung melawan makhluk misterius dan berjuang untuk menjauh dari DMZ. Mereka membangun persahabatan melalui perjuangan mereka.

Anggota pasukan pencarian militer, termasuk Koo Dong-Jin (Jang Dong-Yoon) dan Son Ye-Rim (Krystal) mencoba bertarung melawan makhluk misterius dan berjuang untuk menjauh dari DMZ. Mereka membangun persahabatan melalui perjuangan mereka.
https://akasiafilm.blogspot.com/2020/07/para-pemain-drama-search-menghadiri.html
Drama Search akan ditayangkan di paruh kedua tahun 2020 di channel OCN. Drama ini disutradarai oleh Lim Dae-Woong dan ditulis oleh Goo Mo dan Go Myung-Joo.

Drama ini diperankan oleh aktor dan aktris ternama seperti Jang Dong-yoon, Krystal, Moon Jeong-hee, Yoon Park, Lee Hyun-wook.

Tim produksi berbagi informasi, “Akting penuh gairah para aktor menonjol di set saat mereka dengan sempurna membenamkan diri ke dalam masing-masing karakter mereka meskipun itu menjadi bacaan skrip pertama. Sebuah proyek berkualitas tinggi dengan trifecta dari kisah yang hidup dan terstruktur dengan baik, pengarahan yang tajam, dan akting realistis dari para aktor yang masing-masing menambahkan kehadiran mereka sendiri akan segera mengunjungi pemirsa. ”


https://akasiafilm.blogspot.com